#include "queue.h"
#include <iostream>
using namespace std;

/*
this function init the queue.
input q,size : queue,size to initalize
type q,size : queue,unsigned int
return : void
*/
void initQueue(queue* q, unsigned int size)
{
	q->count = size;
	q->count = 0;
	q->elements = new unsigned int[size];
}
/*
this function clean the queue.
input q : queue
type q : queue
return : void
*/
void cleanQueue(queue* q)
{
	delete[] q->elements;
}
/*
This function does enqueue.
input q,newValue : queue, new value
type q,newValue : queue,unsigned int
return :void
*/
void enqueue(queue* q, unsigned int newValue)
{
	q->elements[q->count] = newValue;
	q->count++;
}
/*
This function does dequeue.
input q : queue
type q : queue
return : q->rear
*/
int dequeue(queue* q)
{

	for (int i = 0; i < q->rear - 1; i++)
	{
		q->elements[i] = q->elements[i + 1];
	}
	q->rear--;
	return q->rear;

}
/*
This function print the queue.
input q : queue
type q : queue
return : void
*/
void print(queue* q)
{
	int i = 0;
	for (i = q->front; i < q->rear; i++)
	{
		printf(" %d <-- ", q->elements[i]);
	}
}


