#include <iostream> 
#include "stack.h"
using namespace std;

/*
this function push an element to stack
input s,element : stsck , element to push
type s,element : stack , unsigned int
return : void
*/
void push(stack* s, unsigned int element)
{
	s->stackHead = insert(s->stackHead, element);
	s->count += 1;
}
/*
this function clean the stack.
input s : stack
type s : stack
return : void
*/
void cleanStack(stack* s)
{
	node* current = s->stackHead;
	node* next;
	while (current != NULL)
	{
		next = current->next;
		delete current;
		current = next;
	}
	delete s;
}
/*
this function pop an element from stack
input s : stack
type s : stack
return : poped 
rtype : int
*/
int pop(stack* s)
{
	int poped = -1, i = 0;
	node* current_node = NULL;

	if (s->count != 0)
	{
		if (s->count == 1)
		{
			poped = s->stackHead->data;
			deleteNode(s->stackHead);
			s->stackHead = NULL;
		}
		else
		{
			for (current_node = s->stackHead; current_node->next->next != NULL; current_node = current_node->next);
			poped = current_node->next->data;
			deleteNode(s->stackHead);
		}
		s->count -= 1;
	}
	return poped;
}
/*
this function init the stack.
input s : stack
type s : stack
return : void
*/
void initStack(stack* s)
{
	s->stackHead = NULL;
	s->count = 0;
}







