#include "Vector.h"
#include <string>
#include <iostream>
#include <vector>
using namespace std;


/*This function implement the = operator*/
Vector& Vector::operator=(const Vector& other)
{
	if (this == &other) // tries to copy the object to itself
	{
		return *this;
	}
	delete[] this->_elements;
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;
	
	
	
	this->_elements = new int[other._capacity];
	for (int i = 0; i < other._capacity; i++)
	{
		this->_elements[i] = other._elements[i];
	}
	return *this;
}

/*This function implement the [] operator and if n go beyond from the vector the func return the first elment of the vector */
int& Vector::operator[](int n) const
{
	if (n > this->_size || n < 0)
	{
		cout << "n go beyond from the vector\n" << endl;
		return this->_elements[0];
	}
	else
	{
		return this->_elements[n];
	}
}

/*constractor*/
Vector::Vector(int n)
{
	if (n < 2)
	{
		this->_elements = new int[2];
		this->_capacity = 2;
		this->_resizeFactor = 2;
	}
	else
	{
		this->_elements = new int[n];
		this->_capacity = n;
		this->_resizeFactor = n;
	}
	
	this->_size = 0;
	
	for (int i = 0; i < n; i++)
	{
		this->_elements[i] = 0;
	}
}

/*destractor vector*/
Vector::~Vector()
{
	delete[]this->_elements;
}

/*this func return size of vector*/
int Vector::size() const
{
	return this->_size;
}

/*this func return capacity of vector*/
int Vector::capacity() const
{
	return this->_capacity;
}

/*this func return resizeFactor of vector*/
int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

/*this func check if the vector is empty*/
bool Vector::empty() const
{
	if (this->_size == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*this func add at the end of elements new element*/
void Vector::push_back(const int& val)
{
	
	if (this->_size == this->_capacity)
	{
		int* temp_vec = new int[this->_capacity+ this->_resizeFactor];//create new array
		for (int i = 0; i < (this->_capacity + this->_resizeFactor); i++)//put zero in the new array
		{
			temp_vec[i] = 0;
		}
		for (int i = 0; i < this->_size; i++)//put _elements in the new vector
		{
			temp_vec[i] = this->_elements[i];
		}
		this->_elements = temp_vec;//convert _elements in the in the new vector 
		this->_capacity += this->_resizeFactor;

		
	}

	this->_elements[this->_size] = val;//add new element
	this->_size++;//increase size 

	
	
}

/*this func remove the last element in elements*/
int Vector::pop_back()
{

	int temp = 0;
	if (this->_size == 0)//the vector is empty
	{
		return -9999;
	}
	else
	{
		int temp = this->_elements[this->_size - 1];
		this->_elements[this->_size - 1] = 0;
		this->_size--;//decrease size
		
	
		return temp;
	}
	
}

/*this func change the capacity of the array by n*/
void Vector::reserve(int n)
{
	if (n > this->_capacity)
	{
		int temp=0;
		for (int i = 1; this->_capacity < n; i++)
		{
			this->_capacity = this->_resizeFactor * i;
		}
		temp = this->_capacity;
		
		int* temp_vec = new int[temp];
		
		for (int i = 0; i < temp; i++)
		{
			temp_vec[i] = 0;
		}
		
		for (int i = 0; i <= this->_size; i++)
		{
			temp_vec[i] = this->_elements[i];
		}
		this->_elements = temp_vec;
	}
}

/*this func change the size of the array by n*/
void Vector::resize(int n)
{
	if (n > this->_capacity)
	{
		reserve(n);
	}
	else
	{
		for (int i = 0; i < this->_size - n; i++)
		{
			pop_back();	
		}
		int* temp_vec = new int[n];
		for (int i = 0; i < n; i++)
		{
			temp_vec[i] = 0;
		}

		for (int i = 0; i < this->_size; i++)
		{
			temp_vec[i] = this->_elements[i];
		}
		this->_elements = temp_vec;
	}
	this->_size = n;
}

/*this func put val in all the elements of the array*/
void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++)//conver all the elements in _elements by val
	{
		this->_elements[i] = val;
	}
}

/*this func change the size of the array by n and put val in all the elements of the array*/
void Vector::resize(int n, const int& val)
{
	resize(n);
	_size = _capacity;
	assign(val);
}

Vector::Vector(const Vector& other)
{
	*this = other;
}