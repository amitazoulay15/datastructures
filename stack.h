#ifndef STACK_H
#define STACK_H
#include "linkedlist.h"

/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	node* stackHead;
	unsigned int count;

} stack;

void push(stack* s, unsigned int element);
int pop(stack* s); // Return -1 if stack is empty
void print(stack* s);
void initStack(stack* s);
void cleanStack(stack* s);


#endif // STACK_H