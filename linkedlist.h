#pragma once
#ifndef LINKEDList_H
#define LINKEDList_H

typedef struct node
{
	unsigned int data;
	node* next;
} node;

node* deleteNode(node* first);
node* insert(node* first, unsigned int value);


#endif 
