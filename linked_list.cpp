#include "iostream"
#include "linkedlist.h"

/*
This function inserd a node to the linked list.
input first,value :  linked list , value to insret to the linked list.
type first,value : node , unsigned int
return : linked list
rtype : node
*/
node* insert(node* first, unsigned int value)
{
	node* new_node = new node;
	node* current_node = NULL;

	new_node->data = value;
	new_node->next = NULL;

	if (first != NULL)
	{
		for (current_node = first; current_node->next != NULL; current_node = current_node->next);
		current_node->next = new_node;
		return first;
	}
	else
	{
		return new_node;
	}
}
/*
This function delete a node from the linked list.
input first : node to delete from the linked list.
type first : node
return first : the linked list
rtype : node
*/
node * deleteNode(node * first)
{
	node* current_node = NULL;
	
	if (first != NULL)
	{
		if (first->next == NULL)
		{
			delete(first);
			first = NULL;
		}
		else
		{
			for (current_node = first; current_node->next->next != NULL; current_node = current_node->next);
			delete(current_node->next);
			current_node->next = NULL;
		}
	}
	return first;
}
